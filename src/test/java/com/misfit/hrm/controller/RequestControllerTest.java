/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misfit.hrm.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.misfit.hrm.HrmApplication;
import com.misfit.hrm.exceptions.PasswordException;
import com.misfit.hrm.repositories.AuthorityRepository;
import com.misfit.hrm.repositories.RequestCategoryRepository;
import com.misfit.hrm.services.EmployeeService;
import com.misfit.hrm.services.EmployeeServiceImpl;
import com.misfit.hrm.web.models.Request;
import com.misfit.hrm.web.models.User;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import org.springframework.util.Base64Utils;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {HrmApplication.class})
@AutoConfigureMockMvc
public class RequestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    private EmployeeService service;

//    @Autowired
//    private TestEntityManager entityManager;
    @Autowired
    private AuthorityRepository authorityRepository;

    @Autowired
    private RequestCategoryRepository requestCategoryRepository;

    User user;

    Request request;

    public RequestControllerTest() {
        //        this.service = new EmployeeServiceImpl();
        user = new User();
        user.setPassword("password");
        user.setConfirmPassword("password");
        user.setProfilePicture("profile picture");
        user.setEmail("demo@misfit.tech");
        user.setUserName("demoUser");
        user.setId(1);
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp(){

//        Employee employee = new Employee();
//        employee.setEmail(user.getEmail());
//        employee.setName(user.getUserName());
//        employee.setProfilePicture(user.getProfilePicture());
//        employee.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
//        Optional<Authority> authority = authorityRepository.findById(AuthorityConst.EMPLOYEE);
//        Set<Authority> authorities = new HashSet<>();
//        authorities.add(authority.get());
//        employee.setAuthorities(authorities);
//        entityManager.persist(employee);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void createRequest() throws Exception {
//        given(service.register(user)).willReturn(user);
        request = new Request();
        request.setTitle("test");
        request.setDescription("test description");
        request.setCategory(requestCategoryRepository.findAll().get(0).getId());

        service.register(user);

        this.mockMvc.perform(post("/request/create")
                .header(HttpHeaders.AUTHORIZATION,
                        "Basic " + Base64Utils.encodeToString((user.getEmail() + ":" + user.getPassword()).getBytes()))
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andDo(print())
                .andExpect(status().isCreated());

    }

    @Test
    public void showRequest() throws Exception {
//        given(service.register(user)).willReturn(user);

        this.mockMvc
                .perform(
                        get("/request/show")
                                .header(HttpHeaders.AUTHORIZATION,
                                        "Basic " + Base64Utils.encodeToString((user.getEmail() + ":" + user.getPassword()).getBytes()))
                )
                .andDo(print())
                .andExpect(status().isOk());

    }
}
