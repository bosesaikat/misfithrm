
insert into authority values('EMPLOYEE'),('HR'),('MANAGER');

insert into request_category values(1,'requisition',true),(2,'leave',false);


insert into employee(email,password,profile_picture,user_name) values('hr@misfit.tech','$2a$10$ALsk5oRbx8nBXZ/hV82AQOVFXkQxlUJ8o6MUypgBtc4lFx60BlfUO','abcd','hr1234');
insert into employee(email,password,profile_picture,user_name) values('manager@misfit.tech','$2a$10$ALsk5oRbx8nBXZ/hV82AQOVFXkQxlUJ8o6MUypgBtc4lFx60BlfUO','abcd','man1234');

insert into employee_authority(id,authority) select id , 'HR' from employee where email='hr@misfit.tech';
insert into employee_authority(id,authority) select id , 'MANAGER' from employee where email='manager@misfit.tech';