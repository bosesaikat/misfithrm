DROP TABLE `employee_authority` IF EXISTS;
DROP TABLE `authority` IF EXISTS;
DROP TABLE `request_category` IF EXISTS;
DROP TABLE `employee` IF EXISTS;

CREATE TABLE `request_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `need_manager_approval_for_processing` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4;

CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `profile_picture` varchar(255) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_user_name` (`user_name`),
  UNIQUE KEY `UK_email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4;

 CREATE TABLE `authority` (
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4;

 CREATE TABLE `employee_authority` (
  `id` int(11) NOT NULL,
  `authority` varchar(255) NOT NULL,
   PRIMARY KEY (`id`,`authority`),
   FOREIGN KEY (`authority`) REFERENCES `authority` (`name`),
  FOREIGN KEY (`id`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4 ;