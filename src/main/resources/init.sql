/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  saikat
 * Created: Oct 14, 2018
 */

DROP TABLE `employee_authority` IF EXISTS;
DROP TABLE `authority` IF EXISTS;
DROP TABLE `request_category` IF EXISTS;
DROP TABLE `employee` IF EXISTS;

CREATE TABLE `request_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `need_manager_approval_for_processing` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `profile_picture` varchar(255) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_user_name` (`user_name`),
  UNIQUE KEY `UK_email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

 CREATE TABLE `authority` (
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

 CREATE TABLE `employee_authority` (
  `id` int(11) NOT NULL,
  `authority` varchar(255) NOT NULL,
  PRIMARY KEY (`id`,`authority`),
  KEY `FK_authority` (`authority`),
  CONSTRAINT `FK_authority` FOREIGN KEY (`authority`) REFERENCES `authority` (`name`),
  CONSTRAINT `FK_employee` FOREIGN KEY (`id`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ;



insert into authority values('EMPLOYEE'),('HR'),('MANAGER');

insert into request_category values(1,'requisition',true),(2,'leave',false);


insert into employee(email,password,profile_picture,user_name) values('hr@misfit.tech','$2a$10$ALsk5oRbx8nBXZ/hV82AQOVFXkQxlUJ8o6MUypgBtc4lFx60BlfUO','abcd','hr1234');
insert into employee(email,password,profile_picture,user_name) values('manager@misfit.tech','$2a$10$ALsk5oRbx8nBXZ/hV82AQOVFXkQxlUJ8o6MUypgBtc4lFx60BlfUO','abcd','man1234');

insert into employee_authority(id,authority) select id , 'HR' from employee where email='hr@misfit.tech';
insert into employee_authority(id,authority) select id , 'MANAGER' from employee where email='manager@misfit.tech';