
package com.misfit.hrm.repositories;

import com.misfit.hrm.entities.RequestStatusLog;
import com.misfit.hrm.entities.RequestStatusLogId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RequestStatusLogRepository extends JpaRepository<RequestStatusLog, RequestStatusLogId> {
}
