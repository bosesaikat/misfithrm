/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misfit.hrm.repositories;

import com.misfit.hrm.constants.RequestStatus;
import com.misfit.hrm.entities.EmployeeRequest;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.springframework.stereotype.Repository;

@Repository
public class RequestCustomRepoImpl implements RequestCustomRepo {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<EmployeeRequest> employeeRequests(int employeeId, Optional<Integer> category, Optional<RequestStatus> status) {
        String queryString = "select r from EmployeeRequest r join r.employee emp";
        if (category.isPresent()) {
            queryString += " join r.category c";
        }
        queryString += " where emp.id =" + employeeId;
        if (category.isPresent()) {
            queryString += " and c.id=" + category.get();
        }
        if (status.isPresent()) {
            queryString += " and r.status='" + status.get().toString()+"'";
        }
        TypedQuery<EmployeeRequest> query = this.entityManager.createQuery(queryString, EmployeeRequest.class);
        return query.getResultList();
    }
    
    @Override
    public List<EmployeeRequest> hrApprovedRequest() {
        String queryString = "select r from EmployeeRequest r where r.status="+RequestStatus.HR_APPROVED;
       
        TypedQuery<EmployeeRequest> query = this.entityManager.createQuery(queryString, EmployeeRequest.class);
        return query.getResultList();
    }

}
