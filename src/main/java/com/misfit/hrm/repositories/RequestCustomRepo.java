/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misfit.hrm.repositories;

import com.misfit.hrm.constants.RequestStatus;
import com.misfit.hrm.entities.EmployeeRequest;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author saikat
 */
public interface RequestCustomRepo {

    List<EmployeeRequest> employeeRequests(int employeeId, Optional<Integer> category, Optional<RequestStatus> status);

    List<EmployeeRequest> hrApprovedRequest();
}
