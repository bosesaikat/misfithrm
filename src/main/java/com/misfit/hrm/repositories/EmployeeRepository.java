
package com.misfit.hrm.repositories;

import com.misfit.hrm.entities.Employee;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface EmployeeRepository extends JpaRepository<Employee, Integer>{
    @Query("SELECT u FROM Employee u WHERE u.userName = ?1")
    List<Employee> findUserByName(String userName);

    @Query("SELECT u FROM Employee u WHERE u.email = ?1")
    List<Employee> findUserByEmail(String email);
}
