package com.misfit.hrm.repositories;

import com.misfit.hrm.entities.RequestCategory;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface RequestCategoryRepository extends JpaRepository<RequestCategory, Integer>{
    @Query("select r from RequestCategory r where r.name =?1")
    List<RequestCategory> findByName(String name);
}
