
package com.misfit.hrm.repositories;

import com.misfit.hrm.entities.EmployeeRequest;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RequestRepository extends JpaRepository<EmployeeRequest, String> , RequestCustomRepo{
}
