/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misfit.hrm.repositories;

import com.misfit.hrm.entities.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author saikat
 */
public interface AuthorityRepository extends JpaRepository<Authority, String>{
    
}
