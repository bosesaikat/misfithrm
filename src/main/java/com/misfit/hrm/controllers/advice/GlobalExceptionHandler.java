package com.misfit.hrm.controllers.advice;

import com.misfit.hrm.exceptions.InvalidEmailException;
import com.misfit.hrm.exceptions.PasswordException;
import com.misfit.hrm.web.models.ErrorMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorMessage> exceptionHandler(Exception ex) {
        LOGGER.error(ex.getLocalizedMessage(), ex);
        ErrorMessage error = new ErrorMessage();
        error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        error.setErrorMessage("Server error");
        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(InvalidEmailException.class)
    public ResponseEntity<ErrorMessage> exceptionHandler(InvalidEmailException ex) {
        LOGGER.error(ex.getLocalizedMessage(), ex);
        ErrorMessage error = new ErrorMessage();
        error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        error.setErrorMessage("Server error");
        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(PasswordException.class)
    public ResponseEntity<ErrorMessage> exceptionHandler(PasswordException ex) {
        LOGGER.error(ex.getLocalizedMessage(), ex);
        ErrorMessage error = new ErrorMessage();
        error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        error.setErrorMessage(ex.getLocalizedMessage());
        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<ErrorMessage> exceptionHandler(AccessDeniedException ex) {
        LOGGER.error(ex.getLocalizedMessage(), ex);
        ErrorMessage error = new ErrorMessage();
        error.setErrorCode(HttpStatus.FORBIDDEN.value());
        error.setErrorMessage(ex.getLocalizedMessage());
        return new ResponseEntity<>(error, HttpStatus.FORBIDDEN);
    }

}
