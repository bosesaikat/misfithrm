package com.misfit.hrm.controllers;

import com.misfit.hrm.exceptions.PasswordException;
import com.misfit.hrm.web.models.User;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.misfit.hrm.services.EmployeeService;
import org.springframework.http.MediaType;

@RestController
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;
    
    @PostMapping(value = "/register", consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody User register(@Valid @RequestBody User user) throws PasswordException{
        return this.employeeService.register(user);
    }
}
