package com.misfit.hrm.controllers;

import com.misfit.hrm.constants.RequestStatus;
import com.misfit.hrm.services.RequestService;
import com.misfit.hrm.web.models.Request;
import java.security.Principal;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/request")
public class RequestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RequestController.class);

    @Autowired
    private RequestService requestService;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "/create")
    public @ResponseBody
    Request createRequest(@RequestBody Request request, Principal principal) throws Exception {
        request.setEmployeeId(Integer.parseInt(principal.getName()));
        String requestId = this.requestService.createRequest(request.getEmployeeId(), request.getTitle(), request.getDescription(), request.getCategory());
        request.setId(requestId);
        return request;
    }

    @GetMapping(value = "/show")
    public @ResponseBody
    List<Request> show(Principal principal, @RequestParam(required = false) Integer category, @RequestParam(required = false) RequestStatus status) throws Exception {
        return this.requestService.employeeRequests(Integer.parseInt(principal.getName()), Optional.ofNullable(category), Optional.ofNullable(status));
    }
    
    @GetMapping(value = "/showAll")
    public @ResponseBody
    List<Request> showAll() throws Exception {
        return this.requestService.requests();
    }
    
    @GetMapping(value = "/hrReviewed")
    public @ResponseBody
    List<Request> hrReviewdRequests() throws Exception {
        return this.requestService.hrReviewedRequests();
    }

    @PostMapping(value = "/hrApproved")
    public @ResponseBody
    ResponseEntity approve(Principal principal, @RequestParam String id) throws Exception {
        return new ResponseEntity(this.requestService.approveRequest(principal, id));
    }

    @PostMapping(value = "/hrProcessed")
    public @ResponseBody
    ResponseEntity processed(Principal principal, @RequestParam String id) throws Exception {
        return new ResponseEntity(this.requestService.proccessedRequest(principal, id));
    }
    
    @PostMapping(value = "/managerApproved")
    public @ResponseBody
    ResponseEntity managerApprove(Principal principal, @RequestParam String id) throws Exception {
        return new ResponseEntity(this.requestService.proccessedRequest(principal, id));
    }

}
