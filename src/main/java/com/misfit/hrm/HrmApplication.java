package com.misfit.hrm;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import org.hibernate.SessionFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@ComponentScan(basePackages = "com.misfit.hrm")
@EnableTransactionManagement
public class HrmApplication {

    static {
        System.setProperty("hibernate.dialect.storage_engine", "innodb");
    }

    public static void main(String[] args) {
        SpringApplication.run(HrmApplication.class, args);
    }

}
