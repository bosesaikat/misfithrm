
package com.misfit.hrm.services;

import com.misfit.hrm.constants.RequestStatus;
import com.misfit.hrm.entities.EmployeeRequest;
import com.misfit.hrm.web.models.Request;
import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.http.HttpStatus;

public interface RequestService {

    String createRequest(int employeeId, String title, String description, int category) throws Exception;

    List<Request> employeeRequests(int employeeId, Optional<Integer> category, Optional<RequestStatus> status);
    
    List<Request> requests();
    List<Request> hrReviewedRequests();
    
    HttpStatus approveRequest(Principal principal, String requestId);
    
    HttpStatus proccessedRequest(Principal principal, String requestId);
    HttpStatus managerApproved(Principal principal, String requestId);
}
