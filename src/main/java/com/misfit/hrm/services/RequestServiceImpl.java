package com.misfit.hrm.services;

import com.misfit.hrm.constants.RequestStatus;
import com.misfit.hrm.entities.Employee;
import com.misfit.hrm.entities.EmployeeRequest;
import com.misfit.hrm.entities.RequestStatusLog;
import com.misfit.hrm.repositories.EmployeeRepository;
import com.misfit.hrm.repositories.RequestCategoryRepository;
import com.misfit.hrm.repositories.RequestRepository;
import com.misfit.hrm.repositories.RequestStatusLogRepository;
import com.misfit.hrm.web.models.Request;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Service;

@Service
@Transactional

public class RequestServiceImpl implements RequestService {

    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private RequestCategoryRepository categoryRepository;
    @Autowired
    private RequestRepository requestRepository;
    @Autowired
    private RequestStatusLogRepository requestStatusLogRepository;

    @Override
    public String createRequest(int employeeId, String title, String description, int category) throws Exception {
        EmployeeRequest request;
        Employee employee = employeeRepository.getOne(employeeId);
        if (employee == null) {
            throw new Exception("User not found");
        }
        request = new EmployeeRequest();
        request.setId(UUID.randomUUID().toString());
        request.setTitle(title);
        request.setDescription(description);
        request.setEmployee(employee);
        request.setStatus(RequestStatus.OPEN);
        request.setCategory(categoryRepository.findById(category).get());
        requestRepository.save(request);
        storeLog(request.getId(), employeeId, RequestStatus.OPEN);

        return request.getId();
    }

    private void storeLog(String requestId, Integer userId, RequestStatus status) {
        RequestStatusLog requestStatusLog = new RequestStatusLog();
        requestStatusLog.setRequestId(requestId);
        requestStatusLog.setUserId(userId);
        requestStatusLog.setStatus(status);
        requestStatusLog.setTime(System.currentTimeMillis());
        this.requestStatusLogRepository.save(requestStatusLog);
    }

    private List<Request> mappRequest(List<EmployeeRequest> employeeRequests) {
        List<Request> requests = new ArrayList<>();
        for (EmployeeRequest employeeRequest : employeeRequests) {
            Request request = new Request();
            request.setId(employeeRequest.getId());
            request.setTitle(employeeRequest.getTitle());
            request.setDescription(employeeRequest.getDescription());
            request.setCategory(employeeRequest.getCategory().getId());
            request.setCategoryName(employeeRequest.getCategory().getName());
            request.setStatus(employeeRequest.getStatus());
            request.setEmployeeId(employeeRequest.getEmployee().getId());
            request.setEmail(employeeRequest.getEmployee().getEmail());
            requests.add(request);
        }
        return requests;

    }

    @Override
    public List<Request> employeeRequests(int employeeId, Optional<Integer> category, Optional<RequestStatus> status) {
        List<EmployeeRequest> employeeRequests = requestRepository.employeeRequests(employeeId, category, status);
        return mappRequest(employeeRequests);
    }

    @PreAuthorize("hasAuthority('HR')")
    @Override
    public HttpStatus approveRequest(Principal principal, String requestId) {
        EmployeeRequest request = requestRepository.getOne(requestId);
        if (request == null) {
            return HttpStatus.NOT_FOUND;
        } else {
            request.setStatus(RequestStatus.HR_APPROVED);
            requestRepository.save(request);
            storeLog(requestId, Integer.parseInt(principal.getName()), RequestStatus.HR_APPROVED);
            return HttpStatus.ACCEPTED;
        }
    }

    @PreAuthorize("hasAuthority('HR')")
    @Override
    public HttpStatus proccessedRequest(Principal principal, String requestId) {
        EmployeeRequest request = requestRepository.getOne(requestId);
        if (request == null) {
            return HttpStatus.NOT_FOUND;
        } else if (request.getCategory().isNeedManagerApprovalForProcessing()) {
            return HttpStatus.FORBIDDEN;
        } else {
            request.setStatus(RequestStatus.PROCESSED);
            requestRepository.save(request);
            storeLog(requestId, Integer.parseInt(principal.getName()), RequestStatus.PROCESSED);
            return HttpStatus.ACCEPTED;
        }
    }

    @PreAuthorize("hasAuthority('HR')")
    @Override
    public List<Request> requests() {
        List<EmployeeRequest> allRequests = requestRepository.findAll();
        return mappRequest(allRequests);
    }

    @PreAuthorize("hasAuthority('MANAGER')")
    @Override
    public HttpStatus managerApproved(Principal principal, String requestId) {
        EmployeeRequest request = requestRepository.getOne(requestId);
        if (request == null) {
            return HttpStatus.NOT_FOUND;
        } else {
            request.setStatus(RequestStatus.PROCESSED);
            requestRepository.save(request);
            storeLog(requestId, Integer.parseInt(principal.getName()), RequestStatus.PROCESSED);
            return HttpStatus.ACCEPTED;
        }
    }

    @PreAuthorize("hasAuthority('MANAGER')")
    @Override
    public List<Request> hrReviewedRequests() {
        List<EmployeeRequest> allRequests = requestRepository.hrApprovedRequest();
        return mappRequest(allRequests);
    }

}
