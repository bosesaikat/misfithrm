package com.misfit.hrm.services;

import com.misfit.hrm.constants.AuthorityConst;
import com.misfit.hrm.entities.Authority;
import com.misfit.hrm.entities.Employee;
import com.misfit.hrm.exceptions.PasswordException;
import com.misfit.hrm.repositories.AuthorityRepository;
import com.misfit.hrm.repositories.EmployeeRepository;
import com.misfit.hrm.web.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import javax.transaction.Transactional;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service("employeeService")
public class EmployeeServiceImpl implements EmployeeService {
    
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private AuthorityRepository authorityRepository;
    @Autowired
    private BCryptPasswordEncoder encoder;
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        
        Integer employeeId ;
        Employee employeeDb = null;
        if(username.endsWith("@misfit.tech")){
            employeeDb = employeeRepository.findUserByEmail(username).get(0);
        }else{
            employeeDb = employeeRepository.findUserByName(username).get(0);
        }
        
        if (employeeDb == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        employeeId = employeeDb.getId();
        
        Collection<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        for (Authority authority : employeeDb.getAuthorities()) {
            GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(authority.getName());
            grantedAuthorities.add(grantedAuthority);
        }
        
        return new org.springframework.security.core.userdetails.User(String.valueOf(employeeDb.getId()), employeeDb.getPassword(), grantedAuthorities);
        
    }
    
    @Override
    @Transactional
    public User register(User user) throws PasswordException {
        if (!user.getPassword().equals(user.getConfirmPassword())) {
            throw new PasswordException("Password mismatched");
        }
        Employee employee = new Employee();
        employee.setEmail(user.getEmail());
        employee.setName(user.getUserName());
        employee.setProfilePicture(user.getProfilePicture());
        employee.setPassword(encoder.encode(user.getPassword()));
        Optional<Authority> authority = authorityRepository.findById(AuthorityConst.EMPLOYEE);
        Set<Authority> authorities = new HashSet<>();
        authorities.add(authority.get());
        employee.setAuthorities(authorities);
        Employee savedEmployee = employeeRepository.save(employee);
        user.setId(savedEmployee.getId());
        return user;
    }
}
