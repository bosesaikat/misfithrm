
package com.misfit.hrm.services;

import com.misfit.hrm.exceptions.PasswordException;
import com.misfit.hrm.web.models.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface EmployeeService extends UserDetailsService{
    public User register(User user) throws PasswordException;
}
