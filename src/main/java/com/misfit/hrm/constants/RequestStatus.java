
package com.misfit.hrm.constants;

public enum RequestStatus {
    OPEN(1),
    HR_APPROVED(2),
    PROCESSED(3),
    DENIED(4),
    
    ;
    private final int value;

    private RequestStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
    
}
