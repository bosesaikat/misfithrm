/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misfit.hrm.entities;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author saikat
 */
public class RequestStatusLogId implements Serializable {

    private String requestId;
    private Integer userId;

    public RequestStatusLogId() {
    }

    public RequestStatusLogId(String requestId, Integer userId) {
        this.requestId = requestId;
        this.userId = userId;
    }

    public String getRequestId() {
        return requestId;
    }

    public Integer getUserId() {
        return userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RequestStatusLogId)) {
            return false;
        }
        RequestStatusLogId that = (RequestStatusLogId) o;
        return Objects.equals(getRequestId(), that.getRequestId())
                && Objects.equals(getUserId(), that.getUserId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRequestId(), getUserId());
    }

}
