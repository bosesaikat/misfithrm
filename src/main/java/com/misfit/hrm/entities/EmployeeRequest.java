package com.misfit.hrm.entities;

import com.misfit.hrm.constants.RequestStatus;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
public class EmployeeRequest {

    @Id
    @NotNull
    private String id;
    @NotNull
    private String title;
    @NotNull
    private String description;
    @ManyToOne
    @JoinColumn(name = "category", nullable = false)
    private RequestCategory category;
    @ManyToOne
    @JoinColumn(name = "employee", nullable = false)
    private Employee employee;

    @NotNull
    @Enumerated(EnumType.STRING)
    private RequestStatus status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public RequestCategory getCategory() {
        return category;
    }

    public void setCategory(RequestCategory category) {
        this.category = category;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public void setStatus(RequestStatus status) {
        this.status = status;
    }

    public RequestStatus getStatus() {
        return status;
    }

}
