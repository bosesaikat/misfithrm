package com.misfit.hrm.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
public class RequestCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @NotNull
    private String name;
    @Column(name = "need_manager_approval_for_processing")
    private boolean needManagerApprovalForProcessing;

    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "request_id")
    private Set<EmployeeRequest> requests;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isNeedManagerApprovalForProcessing() {
        return needManagerApprovalForProcessing;
    }

    public void setNeedManagerApprovalForProcessing(boolean needManagerApprovalForProcessing) {
        this.needManagerApprovalForProcessing = needManagerApprovalForProcessing;
    }

    public Set<EmployeeRequest> getRequests() {
        return requests;
    }

    public void setRequests(Set<EmployeeRequest> requests) {
        this.requests = requests;
    }

}
